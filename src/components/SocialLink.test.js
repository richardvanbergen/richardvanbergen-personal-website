import React from "react"
import { shallow } from "enzyme"

import SocialLink from "./SocialLink"

describe(`Button tests`, () => {
  it(`links to an external site`, () => {
    const component = shallow(
      <SocialLink linkHref="http://example.com" icon={<i />} />
    )
    expect(component.first(`a`).prop(`href`)).toEqual(`http://example.com`)
  })
})
