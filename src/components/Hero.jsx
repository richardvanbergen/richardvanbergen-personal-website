import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"

const Header = styled.h1`
  z-index: 0;
  position: relative;
  font-size: 2em;
  color: ${ props => props.theme.color.text };

  &::after {
    content: "";
    margin: 1rem 0;
    display: block;
    border-bottom: ${ props => props.theme.borderSize } solid
      ${ props => props.theme.color.brand };
  }

  @media (min-width: ${ props => props.theme.breakPoints.tabletPortrait }) {
    font-size: 3em;
  }
`

const HeroContainer = styled.section`
  max-width: 40em;
`

const Copy = styled.div`
  font-size: 1.5em;
`

const Hero = ({ title, children }) => (
  <HeroContainer>
    <Header>{title}</Header>
    {children && <Copy>{children}</Copy>}
  </HeroContainer>
)

Hero.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired
}

export default Hero
