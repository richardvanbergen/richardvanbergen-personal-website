import React, { useState } from "react"
import PropTypes from "prop-types"
import styled, { css, keyframes } from "styled-components"

const bounce = keyframes`
  0% {
    transform: translateY(0);
  }

  50% {
    transform: translateY(1em);
  }

  100% {
    transform: translateY(0);
  }
`

const wiggle = keyframes`
  0% {
    transform: rotate(0);
  }
  
  25% {
    transform: rotate(-10deg);
  }

  75% {
    transform: rotate(10deg);
  }

  100% {
    transform: rotate(0deg);
  }
`

const Icon = styled.img`
  width: 2.5em;
`

const Container = styled.span`
  cursor: pointer;
  text-align: center;
  display: inline-block;
  font-size: 18px;

  ${ ({ animated }) => animated && css`
    ${ Icon } {
      animation-name: ${ wiggle };
      animation-duration: .5s;
      animation-iteration-count: 5;
      animation-timing-function: linear;
    }

    &:hover {
      ${ Icon } {
        animation-name: ${ bounce };
        animation-duration: 1s;
        animation-iteration-count: infinite;
        animation-timing-function: cubic-bezier(0.785, 0.135, 0.15, 0.86);
      }
    }
  ` }

  @media (min-width: ${ props => props.theme.breakPoints.tabletPortrait }) {
    font-size: 30px;
  }
`

const Copy = styled.span`
  font-weight: bold;
  display: block;
  margin-bottom: .5em;
`

const Pointer = ({ text, icon, alt }) => {
  const [animated, setAnimated] = useState(true)
  return (
    <Container animated={animated} onClick={() => setAnimated(false)}>
      <Copy>{text}</Copy>
      <Icon src={icon} alt={alt} />
    </Container>
  )
}

Pointer.propTypes = {
  text: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired
}

export default Pointer
