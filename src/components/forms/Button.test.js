import React from "react"
import { shallow } from "enzyme"

import Button from "./Button"

describe(`Button tests`, () => {
  it(`matches snapshot`, () => {
    expect(shallow(<Button />)).toMatchSnapshot()
  })

  it(`calls function on click`, () => {
    const test = jest.fn()
    const component = shallow(<Button onClick={test} />)
    component.simulate(`click`)
    expect(test.mock.calls).toHaveLength(1)
  })

  it(`uses children as button text`, () => {
    const component = shallow(<Button>Hello!</Button>)
    expect(component.text()).toEqual(`Hello!`)
  })
})
