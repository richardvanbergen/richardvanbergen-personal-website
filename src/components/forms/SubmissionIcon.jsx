import React, {  useState } from 'react'
import PropTypes from 'prop-types'
import styled, { keyframes } from 'styled-components'

const baseRotation = -90
const degreeVariance = 10
const animationSpeed = .8

const wiggle = keyframes`
  0% {
    transform: rotate(${ baseRotation }deg);
  }

  25% {
    transform: rotate(${ baseRotation + degreeVariance }deg);
  }
  
  50% {
    transform: rotate(${ baseRotation }deg);
  }

  75% {
    transform: rotate(${ baseRotation - degreeVariance }deg);
  }

  100% {
    transform: rotate(${ baseRotation }deg);
  }
`

import thumbsUp from '../../images/custom-icons/thumbs-up-regular.svg'

const Icon = styled.img`
  width: 6em;
  margin: 0;
  animation-name: ${ wiggle };
  animation-duration: ${ animationSpeed }s;
  animation-iteration-count: ${ ({ animating }) => animating ? `infinite` : 1 };
  animation-direction: forwards;
  animation-fill-mode: forwards;
  animation-timing-function: linear;
`

const FinalTransitionWrapper = styled.span`
  display: inline-block;
  transform: rotate(0deg);
  transition: transform ${ animationSpeed / 3 }s ease-in;

  ${ ({ isThumbUp }) => {
    if (isThumbUp === null)
      return `transform: rotate(0deg)`
    if (isThumbUp === true)
      return `transform: rotate(90deg)`
    if (isThumbUp === false)
      return `transform: rotate(-90deg)`
  } }
`

const StatusText = styled.span`
  margin-top: .75em;
  font-weight: bold;
  text-transform: uppercase;
  transition: ${ animationSpeed / 3 }s ease-in;

  ${ ({ isThumbUp, theme }) => {
    if (isThumbUp === null)
      return `color: ${ theme.color.text }`
    if (isThumbUp === true)
      return `color: ${ theme.color.textSuccess }`
    if (isThumbUp === false)
      return `color: ${ theme.color.textDanger }`
  } }
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
`

const SubmissionIcon = ({ className, requestStatus }) => {
  const [isThumbUp, setIsThumbUp] = useState(null)

  let success = null
  let animating = true
  let animationEnded = false

  if (requestStatus === `processing`) {
    animating = true
  }

  if ([`successful`, `error`].indexOf(requestStatus) > -1) {
    animating = false
    success = requestStatus === `successful` ? true : false

    // backup plan in case the request took
    // so long the animation didn't finish animating fully
    if (animationEnded) {
      setIsThumbUp(success)
    }
  }

  const handleAnimationEnd = () => {
    animationEnded = true
    setIsThumbUp(success)
  }

  return (
    <Wrapper className={className}>
      <FinalTransitionWrapper isThumbUp={isThumbUp}>
        <Icon
          src={thumbsUp}
          animating={animating}
          onAnimationEnd={handleAnimationEnd} />
      </FinalTransitionWrapper>

      <StatusText isThumbUp={isThumbUp}>
        {isThumbUp === null && `Processing`}
        {isThumbUp === true && `Message Sent`}
        {isThumbUp === false && `Ruh Roh Raggy`}
      </StatusText>
    </Wrapper>
  )
}

SubmissionIcon.propTypes = {
  requestStatus: PropTypes.oneOf([`processing`, `successful`, `error`]).isRequired
}

export default styled(SubmissionIcon)``