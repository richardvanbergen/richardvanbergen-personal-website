import React from 'react'
import { render } from 'enzyme'

import TextInput from './TextInput'
import theme from '../../utils/theme'

const TestElement = props => {
  const passedProps = { ...props, theme }
  return (
    <TextInput {...passedProps} />
  )
}

describe(`Text input`, () => {
  it(`passes props onto its children`, () => {
    const component = render(<TestElement name="hello" />)
    expect(component.find(`input`)).toHaveLength(1)
  })
})