import { css } from 'styled-components'

const input = css`
  background: rgba(0, 0, 0, 0.3);
  width: 100%;
  border: none;
  padding: .5em 1em;
  color: ${ props => props.theme.color.text };

  + span {
    content: '';
    display: block;
    border-bottom: ${ props => props.theme.borderSize } solid ${ props => props.theme.color.brand };
    transform: scaleX(0);
    margin-bottom: .25em;
    transform-origin: left center;
    transition: transform 0.1s ease-out;
  }

  ::placeholder {
    opacity: 0.2
  }

  :focus {
    outline: none;
    background: rgba(0, 0, 0, 0.3);

    + span {
      transform: scaleX(1);
    }
  }
`

const label = css`
  display: inline-block;
  font-weight: bold;
  margin-bottom: .25em;
`

const error = css`
  display: block;
  color: ${ props => props.theme.color.textDanger };
  margin-bottom: .5em;
  font-style: italic;
  font-weight: bold;
  transform: translate3d(0, 0, 0);
  font-size: .8em;
`

const container = css`
  margin-bottom: 1em;
`

export { input, label, error, container }