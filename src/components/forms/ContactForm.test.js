import React from 'react'
import { shallow } from 'enzyme'

import ContactForm from './ContactForm'

describe(`Contact form`, () => {
  it(`matches snapshot`, () => {
    expect(shallow(<ContactForm onSubmit={() => {}} />)).toMatchSnapshot()
  })
})
