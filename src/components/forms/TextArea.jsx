import React from 'react'
import styled from 'styled-components'

import { input, label, error, container } from './inputStyles'

const InputStyled = styled.textarea`
  ${ input }
  vertical-align: top;
`

const Label = styled.label`
  ${ label }
`

const Error = styled.span`
  ${ error }
`

const Container = styled.div`
  ${ container }
`

export default ({ label, error, ...props }) => {
  return (
    <Container>
      {label ? <Label>{label}</Label> : ``}
      <InputStyled {...props} />
      {/* inputs dont have pseudo elements */}
      <span></span>
      {error && <Error>{error}</Error>}
    </Container>
  )
}