import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"

const StyledButton = styled.button`
  color: ${ props => props.theme.color.text };
  color: ${ ({ intent, theme }) => intent === `cta` ? theme.color.background : theme.color.text };
  border:
    ${ props => props.theme.borderSize }
    solid
    ${ ({ theme, intent = `brand` }) => {
    switch (intent) {
    case `brand`: return theme.color.brand
    case `plain`: return theme.color.text
    case `danger`: return theme.color.textDanger
    case `success`:
    case `cta`:
      return theme.color.brand
    }
  } };
  cursor: pointer;

  font-weight: bold;
  background: ${ ({ intent, theme }) => intent === `cta` ? theme.color.textCta : `inherit` };
  padding: 0.5rem 1rem;
  box-shadow: 0 0 0px 0px rgba(0, 0, 0, 0);
  transition: transform 0.3s, box-shadow 0.3s;

  &:hover {
    transform: translate(
      -${ props => props.theme.borderSize },
      -${ props => props.theme.borderSize }
    );
    box-shadow: ${ props => props.theme.borderSize }
      ${ props => props.theme.borderSize } 0px 0px rgba(0, 0, 0, 0.75);
  }
`

const Button = ({ children, intent = `brand`, ...props }) => (
  <StyledButton { ...props } intent={intent}>{children}</StyledButton>
)

Button.propTypes = {
  onClick: PropTypes.func,
  intent: PropTypes.oneOf([`danger`, `cta`, `plain`, `brand`]),
}

export default Button
