import React from 'react'
import PropTypes from 'prop-types'
import { Formik } from 'formik'
import styled from 'styled-components'

import contactFormSchema from '../../utils/formValidation/contactFormSchema'

import TextInput from './TextInput'
import TextArea from './TextArea'
import Button from './Button'

const ContactRow = styled.div`
  display: block;

  @media (min-width: ${ props => props.theme.breakPoints.tabletPortrait }) {
    display: grid;
    grid-gap: 1em;
    grid-template-columns: 1fr 1fr;
  }
`

const ButtonRow = styled.div`
  display: block;

  button[type=reset] {
    display: none;
  }

  @media (min-width: ${ props => props.theme.breakPoints.tabletPortrait }) {
    display: flex;
    justify-content: space-between;

    button[type=reset] {
      display: inline-block;
    }
  }
`

const ContactForm = ({ className, onSubmit }) => {
  return (
    <Formik
      onSubmit={onSubmit}
      validationSchema={contactFormSchema}
      initialValues={{
        name: ``,
        email: ``,
        message: ``
      }}
    >
      {({ handleChange, handleBlur, handleSubmit, isSubmitting, errors, touched }) => (
        <form className={className} onSubmit={handleSubmit} name="contact" data-netlify="true">
          <input type="hidden" name="form-name" value="contact" />

          <ContactRow>
            <TextInput
              label="Name"
              placeholder="John Smith"
              name="name"
              onChange={handleChange}
              onBlur={handleBlur}
              error={errors.name && touched.name ? errors.name : null}
            />

            <TextInput
              label="Contact Email"
              placeholder="john@example.com"
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              error={errors.email && touched.email ? errors.email : null}
            />
          </ContactRow>

          <TextArea
            placeholder="..."
            label="Message"
            name="message"
            rows={10}
            onChange={handleChange}
            onBlur={handleBlur}
          />

          <ButtonRow>
            <Button intent="plain" type="reset" disabled={isSubmitting}>
              Clear Form
            </Button>

            <Button intent="cta" type="submit" disabled={isSubmitting}>
              Send Message
            </Button>
          </ButtonRow>
        </form>
      )}
    </Formik>
  )
}

ContactForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

export default styled(ContactForm)``
