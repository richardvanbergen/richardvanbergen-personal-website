import React from 'react'
import styled from 'styled-components'

import { input, label, error, container } from './inputStyles'

const InputStyled = styled.input`
  ${ input }
`

const Label = styled.label`
  ${ label }
`

const Error = styled.div`
  ${ error }
`

const Container = styled.div`
  ${ container }
`

export default ({ label, error, ...props }) => (
  <Container>
    {label ? <Label>{label}</Label> : ``}
    <InputStyled {...props} />
    {/* inputs dont have pseudo elements, this makes the border at the bottom */}
    <span></span>
    {error && <Error>{error}</Error>}
  </Container>
)