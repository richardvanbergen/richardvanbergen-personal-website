import React from "react"
import PropTypes from "prop-types"
import styled, { css } from "styled-components"

const sharedStyle = css`
  display: table;
  color: ${ props => props.theme.color.text };
  margin-bottom: 1em;

  ${ props => props.underlined && css`
    &::after {
      content: "";
      display: block;
      border-bottom: ${ props => props.borderSize } solid
        ${ props => props.theme.color.brand };
      margin-top: .5em;
    }
  ` }
`

const H2 = styled.h2`
  ${ sharedStyle }
`

const H3 = styled.h3`
  ${ sharedStyle }
`

const H4 = styled.h4`
  ${ sharedStyle }
`

const Heading = ({ underlined, level = `section`, children }) => {
  if (level === `section`) return <H2 underlined={underlined}>{children}</H2>
  if (level === `sub`) return <H3 underlined={underlined}>{children}</H3>
  if (level === `minor`) return <H4 underlined={underlined}>{children}</H4>
}

Heading.propTypes = {
  underlined: PropTypes.bool,
  level: PropTypes.oneOf([`section`, `sub`, `minor`])
}

export default Heading
