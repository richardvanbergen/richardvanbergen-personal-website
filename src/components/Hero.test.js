import React from 'react'
import { render } from 'enzyme'
import { ThemeProvider } from 'styled-components'

import Hero from './Hero'
import theme from '../utils/theme'

describe(`Hero Headings`, () => {
  it(`Renders as a h1`, () => {
    const component = render(
      <ThemeProvider theme={theme}>
        <Hero title="Title" />
      </ThemeProvider>
    )
    expect(component.text()).toEqual(`Title`)
  })

  it(`renders children in p if it exists`, () => {
    const component = render(
      <ThemeProvider theme={theme}>
        <Hero title="Test">
          <p className="test">Body</p>
        </Hero>
      </ThemeProvider>
    )
    expect(component.find(`.test`).text()).toEqual(`Body`)
  })
})
