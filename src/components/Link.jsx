import React from "react"
import styled, { ThemeProvider } from "styled-components"

import theme from "../utils/theme"

const StyledLink = styled.a`
  cursor: pointer;
  color: ${ props => props.theme.color.textHighlight };
  text-shadow: none;
  text-decoration: ${ props => props.theme.color.textHighlight };

  &:hover {
    color: ${ props => props.theme.color.brand };
  }
`

const ThemedLink = ({ children, ...props }) => (
  <ThemeProvider theme={theme}>
    <StyledLink rel="noopener noreferrer" target="_blank" {...props}>
      {children}
    </StyledLink>
  </ThemeProvider>
)

export default ThemedLink
