import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Link from './Link'

const Content = styled.span`
  display: inline-block;

  @media (min-width: ${ props => props.theme.breakPoints.tabletPortrait }) {
    white-space: nowrap;
  }
`

const MediaImage = styled.img`
  display: none;
  padding-right: 1rem;
  margin: 0;
  transition: transform .2s;
  transform: rotate(0deg);

  @media (min-width: ${ props => props.theme.breakPoints.tabletPortrait }) {
    display: inline-block;
  }
`

const Container = styled(Link)`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  line-height: 1.6;
  padding: 1em 0;

  @media (min-width: ${ props => props.theme.breakPoints.tabletPortrait }) {
    padding: 1em;
  }

  &:hover {
    ${ MediaImage } {
      transform: rotate(10deg);
    }
  }
`

const Media = ({ children, image, alt, link, className, ...props }) => (
  <Container href={link} target="_blank" className={className}>
    <MediaImage src={image} alt={alt} {...props} />
    <Content>{children}</Content>
  </Container>
)

Media.propTypes = {
  image: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  className: PropTypes.string
}

export default styled(Media)``