import React from 'react'
import PropTypes from 'prop-types'
import styled, { createGlobalStyle, ThemeProvider } from 'styled-components'
import { Normalize } from 'styled-normalize'

import 'typeface-patua-one'
import 'typeface-montserrat'

import theme from '../utils/theme'

const Globals = createGlobalStyle`
  body {
    background: ${ props => props.theme.color.background };
    color: ${ props => props.theme.color.text };
  }
`

const Footer = styled.footer`
  color: ${ ({ theme }) => theme.color.text };
  background-color: ${ ({ theme }) => theme.color.footer };
  height: 60px;
  line-height: 60px;
  padding-left: 1rem;
  padding-right: 1rem;
`

const Layout = ({ children }) => {
  const date = new Date()
  return (
    <ThemeProvider theme={theme}>
      <>
        {children}
        <Footer>&copy;{date.getFullYear()}</Footer>
        <Normalize />
        <Globals />
      </>
    </ThemeProvider>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired
}

export default Layout
