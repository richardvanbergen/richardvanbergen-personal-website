import React from "react"
import PropTypes from "prop-types"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import styled, { keyframes } from "styled-components"
import ThemedLink from "./Link"

const rotate = keyframes`
  from {
    transform: rotate(0deg) scale(0);
  }

  to {
    transform: rotate(360deg) scale(1);
  }
`

const Link = styled(ThemedLink)`
  font-size: 4rem;
  cursor: pointer;
  display: inline-block;

  svg {
    animation: ${ rotate } 0.5s ${ props => (props.delay ? props.delay : `0`) }s
      cubic-bezier(0.86, 0, 0.07, 1) both;
  }

  div {
    color: ${ props => props.theme.color.textHighlight };
    position: absolute;
    transform: translateY(1.1em);
    transition: transform 0.2s, opacity 0.2s;
    opacity: 0;
    top: 0;
    left: 0;
    width: 100%;
    padding: 0.2rem;

    figcaption {
      font-size: 0.3em;
      line-height: 1;
    }
  }

  @media (max-width: 800px) {
    font-size: 2rem;

    figure > div > figcaption {
      font-size: 0.5em;
      line-height: 1;
    }
  }

  &:hover {
    figure {
      div {
        opacity: 1;
        transform: translateY(1.2em);
      }
    }
  }
`

const SocialLink = ({ children, linkHref, icon, alt = ``, delay = 0 }) => (
  <Link href={linkHref} target="_blank" delay={delay} rel="noopener noreferrer">
    <figure>
      <FontAwesomeIcon icon={icon} alt={alt} />
      <div>
        <figcaption>{children}</figcaption>
      </div>
    </figure>
  </Link>
)

SocialLink.propTypes = {
  linkHref: PropTypes.string.isRequired,
  icon: PropTypes.object.isRequired
}

export default SocialLink
