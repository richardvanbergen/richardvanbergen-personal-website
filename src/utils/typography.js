import Typography from 'tyjs'

import theme from './theme'

export default new Typography({
  headerColor: `white`,
  headerWeight: `normal`,
  bodyColor: `white`,
  baseFontSize: `16px`,
  baseLineHeight: 1.4,
  headerFontFamily: [`Patua One`, `serif`],
  bodyFontFamily: [`Montserrat`, `sans-serif`],
  breakpoints: {
    [`@media screen and (min-width: ${ theme.breakPoints.tabletPortrait })`]: {
      baseFontSize: `20px`,
      scaleRatio: 1.9
    }
  }
})
