const primaryDark = `#000000`
const primaryLight = `white`
const primary = `#212121`
const secondaryDark = `#c7a500`
const secondaryLight = `#A1D336`
const secondary = `#FFFF52`
const danger = `#EC2A2A`

export default {
  color: {
    brand: secondary,
    border: primaryDark,
    background: primary,
    backgroundSecondary: `#476B64`,
    backgroundDark: `#42635c`,
    contactBg: `#476B64`,
    text: primaryLight,
    textHighlight: secondaryLight,
    textSecondary: secondaryDark,
    textDanger: danger,
    textCta: secondaryLight,
    textSuccess: secondaryLight,
    textCtaDark: `#7ea725`,
    footer: `#469A7C`
  },
  breakPoints: {
    mobileLandscape: `480px`,
    tabletPortrait: `600px`,
    tabletLandscape: `768px`,
    desktop: `1024px`,
    desktopLarge: `1140px`
  },
  borderSize: `4px`
}
