import { string, object } from 'yup'

export default object().shape({
  name: string().required(`Enter your name.`),
  email: string().email(`Invalid email.`).required(`Please provide a contact email.`),
})
