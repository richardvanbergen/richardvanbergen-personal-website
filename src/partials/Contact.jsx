import React, { useState } from 'react'
import styled, { css } from 'styled-components'
import { post } from 'axios'
import querystring from 'querystring'

import Media from '../components/Media'
import ContactForm from '../components/forms/ContactForm'
import SubmissionIcon from '../components/forms/SubmissionIcon'

import phoneIcon from '../images/custom-icons/circle-phone.svg'
import mailIcon from '../images/custom-icons/circle-mail.svg'

const AbsoluteCenter = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
`

const ContactFormController = styled.div`
  position: relative;
  opacity: 1;
  flex-grow: 2;

  ${ ContactForm } {
    opacity: 1;
    transition: opacity 0.5s;
  }

  ${ ({ showForm }) => {
    if (!showForm) {
      return css`
        ${ ContactForm } {
          opacity: 0;
        }
      `
    }
  } }
`

const ContactArea = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: ${ ({ theme }) => theme.breakPoints.desktopLarge }) {
    flex-direction: row;
  }
`

const MediaList = styled.div`
  flex-grow: 1;

  @media (min-width: ${ ({ theme }) => theme.breakPoints.desktopLarge }) {
    border-left: 1px solid ${ ({ theme }) => theme.color.backgroundDark };
    margin-left: 1.5em;
    flex-direction: row;
  }

  ${ Media } {
    border-top: 1px solid ${ ({ theme }) => theme.color.backgroundDark };

    &:first-child {
      border-top: none;
    }
  }
`

export default () => {
  const [requestStatus, setRequestStatus] = useState(`processing`)
  const [showForm, setShowForm] = useState(true)

  const reset = (setShowForm, setRequestStatus) => setTimeout(() => {
    setShowForm(true)
    setRequestStatus(`processing`)
  }, 1000)

  const submit = async (values, { setSubmitting }) => {
    setSubmitting(true)
    setShowForm(false)

    try {
      await post(
        `/`,
        querystring.stringify({ 'form-name': `contact`, ...values }),
        {
          headers: { 'Content-Type': `application/x-www-form-urlencoded` },
        }
      )
      
      setRequestStatus(`successful`)
      setSubmitting(false)
      reset(setShowForm, setRequestStatus)
    } catch {
      setRequestStatus(`error`)
      setSubmitting(false)
      reset(setShowForm, setRequestStatus)
    }
  }

  return (
    <ContactArea>
      <ContactFormController showForm={showForm}>
        <ContactForm onSubmit={submit} />
        {!showForm && <AbsoluteCenter>
          <SubmissionIcon requestStatus={requestStatus} />
        </AbsoluteCenter>}
      </ContactFormController>

      <MediaList>
        <Media image={mailIcon} link="mailto:rich.vanbergen@gmail.com" alt="Email Link">
        <>
          <strong>Email</strong><br></br>
          hello@richardvanbergen.com
        </>
        </Media>
        <Media image={phoneIcon} link="tel:+447791776668" alt="Phone Number Link">
        <>
          <strong>Phone Number</strong><br></br>
          (+44) 07791 776 778
        </>
        </Media>
      </MediaList>
    </ContactArea>
  )
}