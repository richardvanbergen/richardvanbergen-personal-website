import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import '@storybook/addon-console'

import Layout from './TestLayout'
import Button from '../components/forms/Button'
import TextInput from '../components/forms/TextInput'
import TextArea from '../components/forms/TextArea'
import ContactForm from '../components/forms/ContactForm'
import SubmissionIcon from '../components/forms/SubmissionIcon'
import Hero from '../components/Hero'
import Heading from '../components/Heading'
import Pointer from '../components/Pointer'
import Media from '../components/Media'

import arrowDownIcon from '../images/custom-icons/arrow-circle-down-solid.svg'
import phoneIcon from '../images/custom-icons/circle-phone.svg'
import mailIcon from '../images/custom-icons/circle-mail.svg'

import theme from '../utils/theme'

storiesOf(`Hero`, module)
  .add(`Plain Hero`, () => (
    <Layout>
      <Hero title="Hello Hero!" />
    </Layout>
  ))
  .add(`With Text`, () => (
    <Layout>
      <Hero title="Hello Hero!">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          Ut viverra placerat neque non ultrices.
          Morbi pulvinar risus eget est congue congue.
        </p>
      </Hero>
    </Layout>
  ))

storiesOf(`Headings`, module)
  .add(`With Underlines`, () => (
    <Layout>
      <Heading underlined level='section'>
        Section Heading
      </Heading>

      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Ut viverra placerat neque non ultrices.
        Morbi pulvinar risus eget est congue congue.
      </p>

      <Heading underlined level='sub'>
        Sub Heading
      </Heading>

      <Heading underlined level='minor'>
        Minor Heading
      </Heading>
    </Layout>
  ))
  .add(`Plain`, () => (
    <Layout>
      <Heading level='section'>
        Section Heading
      </Heading>

      <Heading level='sub'>
        Sub Heading
      </Heading>

      <Heading level='minor'>
        Minor Heading
      </Heading>
    </Layout>
  ))


storiesOf(`Forms`, module)
  .add(`Button with text`, () => (
    <Layout>
      <p>
        <Button onClick={action(`clicked`)}>Hello Button</Button>
      </p>

      <p>
        <Button onClick={action(`clicked`)}>
          <span role="img" aria-label="so cool">
            😀 😎 👍 💯
          </span>
        </Button>
      </p>

      <p>
        <Button intent="danger" onClick={action(`clicked`)}>
          DANGER BUTTON
        </Button>
      </p>

      <p>
        <Button intent="cta" onClick={action(`clicked`)}>
          Call To Action Button
        </Button>
      </p>

      <p>
        <Button intent="plain" onClick={action(`clicked`)}>
          Boring Button
        </Button>
      </p>
    </Layout>
  ))
  .add(`Basic text field`, () => (
    <Layout backgroundColor={theme.color.contactBg}>
      <TextInput type="text" placeholder="Placeholder" label="Hello World!" />
      <TextInput type="text" value="Initial value" />
      <TextInput type="text" value="Error input" error="Something done gone wrong!" />
      <TextInput type="password" />
    </Layout>
  ))
  .add(`Basic textarea`, () => (
    <Layout backgroundColor={theme.color.contactBg}>
      <TextArea placeholder="Placeholder" />
      <TextArea value="Initial value" label="Hello World!" />
    </Layout>
  ))
  .add(`Contact Form Submission Animation`, () => {
    
    setTimeout(() => {
      console.log(values)
      setSubmitting(false)
    }, 500)

    return (
      <Layout>
        <SubmissionIcon requestStatus='processing' />
        <br />
        <SubmissionIcon requestStatus='successful' />
        <br />
        <SubmissionIcon requestStatus='error' />
      </Layout>
    )
  })
  .add(`Contact Form`, () => {
    const submit = (values, { setSubmitting }) => {
      setTimeout(() => {
        console.log(values)
        setSubmitting(false)
      }, 500)
    }

    return (
      <Layout backgroundColor={theme.color.contactBg}>
        <ContactForm onSubmit={submit} />
      </Layout>
    )
  })

storiesOf(`Get in touch pointer`, module)
  .add(`Standard`, () => (
    <Layout>
      <Pointer text="Get In Touch!" icon={arrowDownIcon} alt="Scroll down to the contact form." />
    </Layout>
  ))

storiesOf(`Media components`, module)
  .add(`Icon`, () => (
    <Layout>
      <p>
        <Media image={phoneIcon}>
          <>
            <strong>Phone Number</strong><br></br>
            (+44) 07791 776 778
          </>
        </Media>
      </p>
      <p>
        <Media image={mailIcon}>
          <>
            <strong>Email Address</strong><br></br>
            hello@richardvanbergen.com
          </>
        </Media>
      </p>
    </Layout>
  ))