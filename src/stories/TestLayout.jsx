import React from "react"
import PropTypes from "prop-types"
import styled, { ThemeProvider } from "styled-components"
import { Normalize } from "styled-normalize"

import theme from "../utils/theme"

const Playground = styled.div`
  color: ${ props => props.theme.color.text };
  padding: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100%;
  box-sizing: border-box;
`

const Layout = ({ children, backgroundColor = theme.color.background }) => {
  return (
    <ThemeProvider theme={theme}>
      <Playground style={{ backgroundColor: backgroundColor }}>
        <div>
          {children}
        </div>
        <Normalize />
      </Playground>
    </ThemeProvider>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired
}

export default Layout
