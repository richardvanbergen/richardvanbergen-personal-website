import React from 'react'
import styled from 'styled-components'
import { graphql } from 'gatsby'
import Img from 'gatsby-image'
import { Link } from "react-scroll"

import Hero from '../components/Hero'
import Layout from '../components/Layout'
import Contact from '../partials/Contact'
import Pointer from '../components/Pointer'
import Heading from '../components/Heading'
import Seo from '../components/Seo'

import arrowDownIcon from '../images/custom-icons/arrow-circle-down-solid.svg'

const HighlightedText = styled.span`
  color: ${ props => props.theme.color.textHighlight };
`

import theme from '../utils/theme'

const FullPage = styled.section`
  position: relative;
  background-color: ${ ({ backgroundColor }) => backgroundColor };
  box-sizing: border-box;
  min-height: 100vh;
  padding: 1rem;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  overflow: hidden;
`

const StickyPointer = styled.div`
  position: relative;

  @media (min-height: ${ ({ theme }) => theme.breakPoints.tabletLandscape }) {
    position: absolute;
    bottom: 1em;
  }
`

const ContactPage = styled.div`
  @media (min-width: ${ ({ theme }) => theme.breakPoints.desktop }) {
    p {
      max-width: 70%;
    }
  }

  @media (min-width: ${ ({ theme }) => theme.breakPoints.desktopLarge }) {
    padding: 4em 9em;
  }
`

const StyledImage = styled(Img)`
  z-index: 0;
  position: absolute !important;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
`

const IndexPage = ({ data }) => (
  <Layout>
    <Seo
      title='Home'
      keywords={[`web developer`, `fullstack`, `react`, `javascript`]}
    />
    <FullPage backgroundColor={theme.color.background}>
      <StyledImage fluid={data.mainBg.childImageSharp.fluid} aria-hidden />
      <Hero
        title={
          <>
            Hello. I'm <HighlightedText>Richard Vanbergen</HighlightedText>.
          </>
        }
      >
        <p>
          Full-stack web developer who enjoys building rich and performant
          user experiences.
        </p>
      </Hero>

      <StickyPointer>
        <Link
          to="contact"
          smooth={true}
          duration={500}>
          <Pointer text="Get In Touch!" icon={arrowDownIcon} alt="Scroll down to the contact form." />
        </Link>
      </StickyPointer>
    </FullPage>

    <FullPage backgroundColor={theme.color.backgroundSecondary} id="contact">
      <ContactPage>
        <Heading underlined level='section'>
          Contact Me
        </Heading>

        <p>
          Over the last 10 years I have spent a lot of time with a wide variety of web technologies.
          I'm now turning my focus onto building great user experiences that let people to get to
          the information they need quickly and efficiently.
        </p>

        <p>
          If you think you could use that kind of help feel free to contact me using one of the
          methods below.
        </p>

        <Contact />
      </ContactPage>
    </FullPage>
  </Layout>
)

export default IndexPage

export const pageQuery = graphql`
  query {
    mainBg: file(relativePath: { eq: "custom-icons/main-bg.png" }) {
      childImageSharp {
        fluid(maxWidth: 1140, quality: 70) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }`