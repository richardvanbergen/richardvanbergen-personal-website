import React from "react"
import { Link } from "gatsby"
import Layout from "../components/Layout"
import Seo from "../components/Seo"

const NotFoundPage = () => (
  <Layout>
    <Seo title="404: Not found" />
    <article>
      <h1>NOT FOUND.</h1>
      <p>
        You just visited a page that doesn&apos;t exist. Fortunately,
        there&apos;s only one page on this site.
      </p>
      So you probably want to <Link to="/">go back to the home page.</Link>
    </article>
  </Layout>
)

export default NotFoundPage
