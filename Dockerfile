FROM node:11.9.0-alpine
WORKDIR /app
EXPOSE 9000
COPY . .

RUN apk --no-cache --virtual build-dependencies add \
    python \
    make \
    g++ \
    && yarn install \
    && apk del build-dependencies

RUN yarn build
CMD yarn serve