module.exports = {
  siteMetadata: {
    title: `Richard Vanbergen - Fullstack Web Developer`,
    description: `Full Stack Web Developer based out of London. Focus on performance and reliability.`,
    author: `@enrichit`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${ __dirname }/src/images`,
      },
    },
    `gatsby-plugin-eslint`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-offline`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Full Stack Web Developer`,
        short_name: `Web Dev`,
        start_url: `/`,
        background_color: `#212121`,
        theme_color: `#FFFF52`,
        display: `standalone`,
        icon: `src/images/r-icon.png`,
      },
    },
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      }
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: 'UA-132825257-1',
      },
    },
  ],
}
