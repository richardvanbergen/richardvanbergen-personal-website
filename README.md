# Welcome To My Personal Website

This is a gatsby application. You can run it by doing all the gastby things you're used to doing. But just in case:

    yarn
    yarn start

I got a couple of other ticks though!

## Styled Components

The application is styled using [styled components](https://www.styled-components.com/) which are amazing.

## Typography.js

It uses [tyjs](https://www.npmjs.com/package/tyjs) which is a temporary fork of [Typography.js](https://kyleamathews.github.io/typography.js/) due to not [supporting break points](https://github.com/KyleAMathews/typography.js/issues/75) yet.

## Storybook

Check out the components with `yarn storybook`.

## Docker

The entire application is Docker-ised. There's a [CI/CD pipeline](https://gitlab.com/richardvanbergen/richardvanbergen-personal-website/pipelines) to build the application and trigger deployments on the master branch. I'm looking into kubernetes because GitLab has nice support for that but I'm not there yet.

## AWS - SAM, Api Gateway & Lambda

The contact form uses [AWS SAM](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html) to build out my lambda-based API. You can find this under `services`.

## Final Notes

The current iteration is not [quite finished yet](https://gitlab.com/richardvanbergen/richardvanbergen-personal-website/boards) but it's 95% of the way and I'm happy enough to send it out. 

The site does pretty well on a lighthouse audit but [it's not good enough](https://gitlab.com/richardvanbergen/richardvanbergen-personal-website/issues/30)!