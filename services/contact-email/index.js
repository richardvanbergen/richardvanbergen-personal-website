const AWS = require(`aws-sdk`)

const { string, object } = require(`yup`)
const validationSchema = object().shape({
  name: string().required(`Enter your name.`),
  email: string().email(`Invalid email.`).required(`Please provide a contact email.`)
})

AWS.config.update({ region: `eu-west-1` })

module.exports = (name, message, email, success, error) => {
  success(process.env)
  // validationSchema.isValid({
  //   name,
  //   message,
  //   email
  // })
  //   .then(valid => {
  //     if (!valid) {
  //       return error(`Server validation failed.`)
  //     }

  //     let params = {
  //       Destination: {
  //         CcAddresses: [
  //         ],
  //         ToAddresses: [
  //           `hello@richardvanbergen.com`,
  //         ]
  //       },
  //       Message: {
  //         Body: {
  //           Html: {
  //             Charset: `UTF-8`,
  //             Data: message
  //           },
  //           Text: {
  //             Charset: `UTF-8`,
  //             Data: message
  //           }
  //         },
  //         Subject: {
  //           Charset: `UTF-8`,
  //           Data: `New contact form submission from ${ name } (${ email })`
  //         }
  //       },
  //       Source: `hello@richardvanbergen.com`,
  //       ReplyToAddresses: [
  //         email,
  //       ],
  //     }

  //     let sendPromise = new AWS.SES({ apiVersion: `2010-12-01` }).sendEmail(params).promise()

  //     sendPromise
  //       .then(data => {
  //         return success({ messageId: data.MessageId })
  //       })
  //       .catch(err => {
  //         return error(err.message)
  //       })
  //   })
}
