const contactEmail = require(`./contact-email/index`)

exports.sendContactEmail = (event, context, callback) => {
  const { email, name, message } = JSON.parse(event.body)

  const done = (err, res) => {
    return callback(null, {
      statusCode: err ? 500 : 200,
      body: err ? err : JSON.stringify(res),
      headers: {
        'Content-Type': `application/json`,
        'Access-Control-Allow-Origin': `*`
      },
    })
  }

  contactEmail(name, message, email, message => done(null, message), error => done(error, null))
}