module.exports = {
  extends: ["prettier"],
  plugins: ["prettier", "react", "jest"],
  rules: {
    "jest/no-disabled-tests": "warn",
    "jest/no-focused-tests": "error",
    "jest/no-identical-title": "error",
    "jest/prefer-to-have-length": "warn",
    "jest/valid-expect": "error",
    "object-curly-spacing": ["error", "always"],
    "no-var": "error", // optional, recommended when using es6+
    "no-unused-vars": 1, // recommended
    "arrow-spacing": ["error", { before: true, after: true }], // recommended
    "quotes": ["error", "backtick"],
    indent: ["error", 2],
    "comma-dangle": [
      "error",
      {
        objects: "only-multiline",
        arrays: "only-multiline",
        imports: "never",
        exports: "never",
        functions: "never"
      }
    ],

    // options to emulate prettier setup
    semi: ["error", "never"],
    "max-len": ["error", { code: 120 }],
    "template-curly-spacing": ["error", "always"],
    "arrow-parens": ["error", "as-needed"],

    // react plugin - options
    "react/jsx-uses-react": "error",
    "react/jsx-uses-vars": "error"
  },
  parser: "babel-eslint",
  parserOptions: {
    ecmaVersion: 8 // optional, recommended 6+
  },
  env: {
    "jest/globals": true
  }
};