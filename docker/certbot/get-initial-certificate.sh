[ ! -z $(docker images -q nginx:alpine) ] || (
  docker volume create certbot &&  \
  docker volume create keys && \
  docker-compose up -d && \
  docker-compose down
)